package com.twuc.webApp.service;

import com.twuc.webApp.enums.OperationEnum;
import com.twuc.webApp.model.Operation;
import org.springframework.stereotype.Service;

@Service
public class TableService {

    public String plus() {
        return this.plus(1, 9);
    }

    public String plus(Integer start, Integer end) {
        if (isArgumentIllegal(start, end)) {
            throw new IllegalArgumentException();
        }
        return calculate(OperationEnum.ADD.getOperation(), start, end);
    }

    public String multiply() {
        return this.multiply(1, 9);
    }

    public String multiply(Integer start, Integer end) {
        if (isArgumentIllegal(start, end)) {
            throw new IllegalArgumentException();
        }
        return calculate(OperationEnum.MULTIPLY.getOperation(), start, end);
    }

    private String calculate(String operation, Integer start, Integer end) {
        StringBuilder result = new StringBuilder();
        for (int i = start; i <= end; i++) {
            for (int j = 1; j <= i; j++) {
                int res = operation.equals("+") ? i + j : i * j;
                String format = String.format("%d" + operation + "%d=%d", i, j, res);
                result.append(format);
                if (i != j) {
                    result.append(res >= 10 ? " " : "  ");
                }
            }
            result.append("\n");
        }
        return String.valueOf(result);
    }

    private boolean isArgumentIllegal(Integer start, Integer end) {
        return start < 1 || end < 1 || start > 10 || end > 10;
    }

    public boolean check(Operation operation) {
        String operations = operation.getOperation();
        Integer operationExpect = operation.getExpectedResult();
        if (!operation.getCheckType().equals("=")) {
            if (OperationEnum.ADD.getOperation().equals(operations)) {
                return ">".equals(operation.getCheckType()) ? operation.getOperandLeft() + operation.getOperandRight() > operationExpect :
                        operation.getOperandLeft() + operation.getOperandRight() < operationExpect;
            }
            if (OperationEnum.MULTIPLY.getOperation().equals(operations)) {
                return ">".equals(operation.getCheckType()) ? operation.getOperandLeft() * operation.getOperandRight() > operationExpect :
                        operation.getOperandLeft() * operation.getOperandRight() < operationExpect;
            }
        } else {
            return OperationEnum.ADD.getOperation().equals(operations) ? operation.getOperandLeft() + operation.getOperandRight() == operationExpect :
                    operation.getOperandLeft() * operation.getOperandRight() == operationExpect;
        }
        return true;
    }
}
