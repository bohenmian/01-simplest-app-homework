package com.twuc.webApp.web;

import com.twuc.webApp.model.Operation;
import com.twuc.webApp.service.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class TableController {

    private TableService tableService;

    @Autowired
    public TableController(TableService tableService) {
        this.tableService = tableService;
    }

    @GetMapping("/tables/plus")
    public String plus() {
        return tableService.plus();
    }

    @GetMapping("/tables/multiply")
    public String multiply() {
        return tableService.multiply();
    }

    @GetMapping("/tables/plusNumber")
    public String plusWithNumber(@RequestParam("start") Integer start, @RequestParam("end") Integer end) {
        return tableService.plus(start, end);
    }

    @GetMapping("/tables/multiplyNumber")
    public String multiplyWithNumber(@RequestParam Integer start, @RequestParam Integer end) {
        return tableService.multiply(start, end);
    }

    @PostMapping("/check")
    public ResponseEntity<Boolean> check(@RequestBody Operation operation) {
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(tableService.check(operation));
    }
}
