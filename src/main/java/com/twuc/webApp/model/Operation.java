package com.twuc.webApp.model;



import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

public class Operation {

    @NotEmpty
    private Integer operandLeft;
    @NotEmpty
    private Integer operandRight;
    @NotBlank
    private String operation;
    @NotEmpty
    private Integer expectedResult;

    private String checkType;


    public Operation() {
    }

    public Operation(Integer operandLeft, Integer operandRight, String operation, Integer expectedResult, String checkType) {
        this.operandLeft = operandLeft;
        this.operandRight = operandRight;
        this.operation = operation;
        this.expectedResult = expectedResult;
        this.checkType = checkType;
    }

    public Operation(Integer operandLeft, Integer operandRight, String operation, Integer expectedResult) {
        this(operandLeft, operandRight, operation, expectedResult, "=");
    }

    public Integer getOperandLeft() {
        return operandLeft;
    }

    public Integer getOperandRight() {
        return operandRight;
    }

    public String getOperation() {
        return operation;
    }

    public Integer getExpectedResult() {
        return expectedResult;
    }

    public String getCheckType() {
        return checkType;
    }
}
