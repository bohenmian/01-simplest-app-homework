package com.twuc.webApp.enums;

public enum OperationEnum {

    ADD("+"),
    MULTIPLY("*");

    private String operation;

    OperationEnum(String operation) {
        this.operation = operation;
    }

    public String getOperation() {
        return operation;
    }
}
